//
//  FlickrImageCell.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import UIKit

class FlickImageCell: UICollectionViewCell {
    var photo = UIImageView()
    var title = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        photo.image = nil
        title.text = nil
    }
    
    func configureCell(with flikrPhoto: FlickrPhoto) {
        photo.load(imageUrl: flikrPhoto.url)
        title.text = flikrPhoto.title
    }
    
    func setupViews() {
        photo.translatesAutoresizingMaskIntoConstraints = false
        title.translatesAutoresizingMaskIntoConstraints = false
        
        photo.layer.borderColor = UIColor.white.cgColor
        photo.layer.borderWidth = 2.0
        
        title.numberOfLines = 1
        photo.contentMode = .scaleAspectFill
        photo.clipsToBounds = true
        photo.adjustsImageWhenAncestorFocused = true
        
        contentView.addSubview(photo)
        contentView.addSubview(title)
    }
    
    func setupConstraints() {
        let margin: Double = 8.0
        
        photo.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        photo.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        photo.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        photo.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        title.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: margin).isActive = true
        title.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -margin).isActive = true
        title.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -margin).isActive = true
    }
}

