//
//  FlickrCollectionView.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import UIKit

class FlickrFeedsViewController: UIViewController {
    
    private let collectionView: UICollectionView = UICollectionView.getDefault()
    private var flickrImages = [FlickrPhoto]()
    private var flagFlickrMethod: FlickrMethod = .popular
    private var selectedFlickrImageIndex: Int?
    private var nextFlickrPage: Int = 1
    private let thresholdForPrefetch = 50
    private var isFetchingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let flickerSearchTerm = UserDefaults.standard.object(forKey: Constants.flickrSearchTerm) as? String {
            if case .search(let savedSearchText) = flagFlickrMethod, savedSearchText ==  flickerSearchTerm {
                return
            } else {
                nextFlickrPage = 1
            }
            
            flagFlickrMethod = .search(text: flickerSearchTerm)
        } else if case .popular = flagFlickrMethod, !flickrImages.isEmpty {
            return
        }
        
        downloadData(with: flagFlickrMethod)
    }
    
    private func downloadData(with method: FlickrMethod) {
        let success: (([FlickrPhoto]) -> ()) = { [weak self] photos in
            if self?.nextFlickrPage == 1 {
                self?.flickrImages.removeAll()
            }
            
            self?.flickrImages.append(contentsOf: photos)
            self?.isFetchingData = false
            self?.nextFlickrPage += 1
            
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
        
        let failure: (() -> ()) = { [weak self] in
            // TODO: On error log on server
            self?.isFetchingData = false
        }
        
        if !isFetchingData {
            isFetchingData = true
            APIService.shared.callService(method: method,
                                          page: nextFlickrPage,
                                          success: success,
                                          failure: failure)
        }
    }
    
    private func setupUI() {
        view.backgroundColor = .black
        setupCollectionView()
        setupConstraints()
    }
    
    private func setupCollectionView() {
        view.addSubview(collectionView)
        
        collectionView.register(FlickImageCell.self, forCellWithReuseIdentifier: "FlickrImageCell")
        setupDelegateAndDataSource()
    }
    
    private func setupDelegateAndDataSource() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.prefetchDataSource = self
    }
    
    private func setupConstraints() {
        collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8.0).isActive = true
        collectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8.0).isActive = true
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail",
           let flickrImageViewController = segue.destination as? FlickrImageViewController,
            let selectedImageIdex = selectedFlickrImageIndex {
            
            flickrImageViewController.selectedFlickrPhoto = selectedImageIdex
            flickrImageViewController.flikrPhotos = flickrImages
        }
    }
}

extension FlickrFeedsViewController: UICollectionViewDelegate,
                                     UICollectionViewDataSource,
                                     UICollectionViewDelegateFlowLayout,
                                     UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FlickrImageCell", for: indexPath) as? FlickImageCell {
            let flickImage = flickrImages[indexPath.row]
            cell.configureCell(with: flickImage)
            return cell
        } else {
            return FlickImageCell()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        flickrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedFlickrImageIndex = indexPath.row
        performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: (collectionView.frame.width / 3) - 32.05, height: ((collectionView.frame.width / 3) - 32.05) * (9 / 16))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        16
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if indexPath.row > flickrImages.count - thresholdForPrefetch {
                downloadData(with: flagFlickrMethod)
            }
        }
    }
}
