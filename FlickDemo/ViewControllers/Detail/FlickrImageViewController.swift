//
//  FlickrImageViewController.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import UIKit

class FlickrImageViewController: UIViewController {
    var flikrPhotos: [FlickrPhoto]?
    var photoImageView = UIImageView()
    var selectedFlickrPhoto: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupImageView()
        setupConstraints()
        setupGesture()
    }
    
    func setupImageView() {
        photoImageView.translatesAutoresizingMaskIntoConstraints = false
        photoImageView.contentMode = .scaleAspectFill
        photoImageView.clipsToBounds = true
        view.addSubview(photoImageView)
        
        guard let numberOfImages = flikrPhotos?.count, numberOfImages > 0 else { return }
        
        photoImageView.load(imageUrl: flikrPhotos?[selectedFlickrPhoto].fullImageUrl)
    }
    
    func setupConstraints() {
        photoImageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        photoImageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        photoImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        photoImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    func setupGesture() {
        let leftSwipeGestureRecognizerDown = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        leftSwipeGestureRecognizerDown.direction = .left
        
        let rightSwipeGestureRecognizerDown = UISwipeGestureRecognizer(target: self, action: #selector(didSwipe(_:)))
        rightSwipeGestureRecognizerDown.direction = .right
        
        view.addGestureRecognizer(leftSwipeGestureRecognizerDown)
        view.addGestureRecognizer(rightSwipeGestureRecognizerDown)
    }
    
    @objc private func didSwipe(_ sender: UISwipeGestureRecognizer) {
        guard let numberOfImages = flikrPhotos?.count, numberOfImages > 0 else { return }
        
        switch sender.direction {
        case .left:
            if selectedFlickrPhoto > 0 {
                selectedFlickrPhoto -= 1
            }
        case .right:
            if selectedFlickrPhoto < numberOfImages - 1 {
                selectedFlickrPhoto += 1
            }
        default:
            return
        }
        
        photoImageView.load(imageUrl: flikrPhotos?[selectedFlickrPhoto].fullImageUrl)
    }
}
