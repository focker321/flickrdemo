//
//  SearchViewController.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import UIKit

class SearchViewController: UIViewController {
    var searchTextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSearchTextField()
        
    }
    
    func setupSearchTextField() {
        setupUI()
        setupConstraints()
    }
    
    func setupUI() {
        searchTextField.delegate = self
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.backgroundColor = .gray
        searchTextField.textColor = .white
        searchTextField.placeholder = "Search"
        view.addSubview(searchTextField)
    }
    
    func setupConstraints() {
        searchTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        searchTextField.topAnchor.constraint(equalTo: view.topAnchor, constant: 200).isActive = true
        searchTextField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        searchTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
}

extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UserDefaults.standard.set(textField.text, forKey: Constants.flickrSearchTerm)
        textField.text = ""
        self.tabBarController?.selectedIndex = 0
        
        return true
    }
}
