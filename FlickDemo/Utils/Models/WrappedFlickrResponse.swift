//
//  WrappedFlickrResponse.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import Foundation

struct RootWrappedFlickrResponse<Wrapped: Decodable>: Decodable {
    var photos: WrappedFlickrResponse<Wrapped>
}

struct WrappedFlickrResponse<Wrapped: Decodable>: Decodable {
    var page: Int
    var pages: Int
    var perpage: Int
    var total: Int
    var photo: Wrapped
}
