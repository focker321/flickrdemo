//
//  FlickrPhoto.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import Foundation

struct FlickrPhoto: Codable {
    var id: String
    var owner: String
    var secret: String
    var server: String
    var farm: Int
    var title: String
    var ispublic: Int
    var isfriend: Int
    var isfamily: Int
}

extension FlickrPhoto {
    var url: URL? {
        getImageUrl(size: "z")
    }
    
    var fullImageUrl: URL? {
        getImageUrl(size: "b")
    }
    
    private func getImageUrl(size: String) -> URL? {
        URL(string: "https://live.staticflickr.com/\(server)/\(id)_\(secret)_\(size).jpg")
    }
}
