//
//  APIService.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import Foundation

class APIService {
    static let shared = APIService()
    
    private var urlSession = URLSession.shared
    
    private init() {}
    
    func callService<T: Decodable>(method: FlickrMethod, page: Int = 1, success: @escaping ((T) -> ()), failure: @escaping (() -> ())) {
        guard let url = flickServiceUrl(for: method, page: page) else { return }
        
        let task = urlSession.dataTask(with: url) { data, response, error in
            guard let unwrapData = data, error == nil else {
                failure()
                return
            }
            
            guard let dataModel = try? JSONDecoder().decode(RootWrappedFlickrResponse<T>.self, from: unwrapData) else {
                failure()
                return
            }
            
            success(dataModel.photos.photo)
        }
        
        task.resume()
    }
}

extension APIService {
    private var flickrBaseURL: String { "https://www.flickr.com/services/rest/?" }
    
    private func flickServiceUrl(for method: FlickrMethod, page: Int) -> URL? {
        let stringURL = "\(flickrBaseURL)method=\(method.methodName)&api_key=\(Constants.flickrApiKey)&format=json&nojsoncallback=1&page=\(page)"
        return URL(string: stringURL)
    }
}

enum FlickrMethod {
    case search(text: String)
    case popular
    
    var methodName: String {
        switch self {
        case .search(let text):
            return "flickr.photos.search&text=\(text)"
        case.popular:
            return "flickr.photos.getPopular&user_id=\(Constants.contactUserId)"
        }
    }
    
}
