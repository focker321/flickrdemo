//
//  UIImageView.swift
//  FlickDemo
//
//  Created by Santiago Delgado on 5/12/21.
//

import UIKit

// MARK: - URL
extension UIImageView {
    // TODO: Improve using a hash for avoiding download several times the same image
    func load(imageUrl: URL?) {
        self.image = nil
        
        guard let url = imageUrl else {
            // TODO: if error show placeholder
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url),
               let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self?.image = image
                    self?.setGradient()
                }
            }
        }
    }
}

// MARK: - Gradient
extension UIImageView {
    func setGradient() {
        let width = self.bounds.width
        let height = self.bounds.height
        let sHeight: CGFloat = 100.0
        let shadow = UIColor.black.cgColor
        
        let bottomImageGradient = CAGradientLayer()
        bottomImageGradient.frame = CGRect(x: 0, y: height - sHeight, width: width, height: sHeight)
        bottomImageGradient.colors = [UIColor.clear.cgColor, shadow]
        self.layer.insertSublayer(bottomImageGradient, at: 0)
    }
}
